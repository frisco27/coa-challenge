﻿using coa.api.Core;
using coa.api.Core.Domain;
using coa.api.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace coa.api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsuariosController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;
        public UsuariosController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var usuarios = _unitOfWork.Usuarios.GetAll();
            return Ok(usuarios);
        }

        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            var usuario = _unitOfWork.Usuarios.Get(id);
            return Ok(usuario);
        }

        [HttpPut]
        public ActionResult Put([FromBody] UsuarioViewModel model)
        {
            var usuarioExistente = _unitOfWork.Usuarios.Get(model.Id);

            if (usuarioExistente != null)
            {
                usuarioExistente.Nombre = model.Nombre;
                usuarioExistente.Email = model.Email;
                usuarioExistente.Telefono = model.Telefono;

                _unitOfWork.Usuarios.Update(usuarioExistente);
                _unitOfWork.Complete();
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error actualizando datos");
            }
            return Ok();
        }

        [HttpPost]
        public ActionResult Post([FromBody] UsuarioViewModel model)
        {
            try
            {
                if (model == null)
                    return BadRequest();

                Usuario nuevoUsuario = new Usuario()
                {
                    Nombre = model.Nombre,
                    Email = model.Email,
                    Telefono = model.Telefono
                };

                _unitOfWork.Usuarios.Add(nuevoUsuario);
                _unitOfWork.Complete();

                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error creando nuevo usuario");
            }
        }

        [HttpDelete("{id:int}")]
        public ActionResult Delete(int id)
        {
            try
            {
                var usuarioExistente = _unitOfWork.Usuarios.Get(id);

                if (usuarioExistente == null)
                {
                    return NotFound($"Usuario con Id = {id} no encontrado.");
                }

                _unitOfWork.Usuarios.Remove(usuarioExistente);
                _unitOfWork.Complete();

                return Ok();

            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error eleminando informacion");
            }
        }
    }
}

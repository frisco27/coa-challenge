using coa.api.Core;
using coa.api.Core.Repositories;
using coa.api.Persistence;
using coa.api.Persistence.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace coa.challenge
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<CoaContext>(options =>
                options.UseSqlServer(
                Configuration.GetConnectionString("DefaultConnection"),
                b => b.MigrationsAssembly(typeof(CoaContext).Assembly.FullName))
            );

            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient<IUsuarioRepository, UsuarioRepository>();

            services.AddTransient<IUnitOfWork, UnitOfWork>();
            RegisterSwagger(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/flusiV1API/swagger.json", "Coa Challenge API");
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void RegisterSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                var groupName = "flusiV1API";

                options.SwaggerDoc(groupName, new OpenApiInfo
                {
                    Title = $"COA {groupName} - CHALLENGE - by Francisco Lusi",
                    Version = groupName,
                    Description = "Coa Challenge API",
                    Contact = new OpenApiContact
                    {
                        Name = "Francisco Lusi",
                        Email = "francisco.lusi.dev@gmail.com",
                        Url = new Uri("https://www.linkedin.com/in/francisco-lusi/"),
                    }
                });
            });
        }

    }
}

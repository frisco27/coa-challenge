using coa.api.Core.Repositories;

using System;

namespace coa.api.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IUsuarioRepository Usuarios { get; }
        int Complete();
    }
}
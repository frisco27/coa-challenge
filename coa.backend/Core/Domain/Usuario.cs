using System.Collections.Generic;

namespace coa.api.Core.Domain
{
    public class Usuario
    {
        public Usuario()
        {

        }
        public int Id { get; set; }

        public string Nombre { get; set; }

        public string Email { get; set; }

        public string Telefono { get; set; }

    }
}

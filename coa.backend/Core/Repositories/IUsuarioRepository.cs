using coa.api.Core.Domain;

namespace coa.api.Core.Repositories
{
    public interface IUsuarioRepository : IRepository<Usuario>
    {
        // interfaz para implementar logica propia de la entidad usuario
    }
}
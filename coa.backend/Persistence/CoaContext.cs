using coa.api.Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace coa.api.Persistence
{
    public class CoaContext : DbContext
    {
        public virtual DbSet<Usuario> Usuarios { get; set; }

        public CoaContext(DbContextOptions<CoaContext> options) : base(options)
        {
        }

    }
}

﻿using coa.api.Core;
using coa.api.Core.Repositories;
using coa.api.Persistence.Repositories;

namespace coa.api.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CoaContext _context;

        public UnitOfWork(CoaContext context)
        {
            _context = context;
            Usuarios = new UsuarioRepository(_context);
        }

        public IUsuarioRepository Usuarios { get; private set; }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
﻿using coa.api.Core.Domain;
using coa.api.Core.Repositories;
using coa.api.Persistence;
using coa.api.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;

using System.Linq;

namespace coa.api.Persistence.Repositories
{
    public class UsuarioRepository : Repository<Usuario>, IUsuarioRepository
    {
        public UsuarioRepository(CoaContext context) : base(context)
        {
        }

        public Usuario Get(int id)
        {
            return CoaContext.Usuarios.Where(usr => usr.Id == id).FirstOrDefault();
        }

        public CoaContext CoaContext
        {
            get { return Context as CoaContext; }
        }
    }
}